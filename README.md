# devops-web-test
Selenium Test Scripts for devops-web project

Usage:
1. Run the ant build using build.xml on the ./build/ folder
2. It generates a jar file in ./build/test.jar
3. Run the below command with desired parameters
# java -jar build/test.jar WINDOWS FIREFOX
# java -jar build/test.jar WINDOWS CHROME
# java -jar build/test.jar LINUX FIREFOX
# java -jar build/test.jar LINUX CHROME

## Maintainers

- [Sudheer Veeravalli](https://github.com/veersudhir83)
- [Sudheer Veeravalli](mailto:veersudhir83@gmail.com)
